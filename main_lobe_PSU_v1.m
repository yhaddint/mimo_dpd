clear;clc;clf;close all
N_range=[4, 16, 64, 256];
N_num = length(N_range);

runtimes = 1e4;
main_lobe_pow_data_normal = zeros(runtimes,N_num,15);
main_lobe_pow_data_uniform = zeros(runtimes,N_num,15);
main_lobe_pow_normal = zeros(N_num,15);
main_lobe_pow_uniform = zeros(N_num,15);

for runindex=1:runtimes
    theta = rand(1,2)*pi;
    for nn=1:N_num
        N = N_range(nn);
        for dd = 1:15
            deltaphi0 = dd/180*pi;
            main_lobe_pow_data_normal(runindex,nn,dd) = abs(sum(exp(j*randn(1,N)*deltaphi0)))/N;
            main_lobe_pow_data_uniform(runindex,nn,dd) = abs(sum(exp(j*(rand(1,N)*2*deltaphi0-deltaphi0))))/N;
            main_lobe_angle_data_normal(runindex,nn,dd) = angle(sum(exp(j*randn(1,N)*deltaphi0)));
            main_lobe_angle_data_uniform(runindex,nn,dd) = angle(sum(exp(j*(rand(1,N)*2*deltaphi0-deltaphi0))));

        end
    end
end
%%
for nn=1:N_num
    for dd=1:15
        main_lobe_pow_normal(nn,dd) = sum(squeeze(main_lobe_pow_data_normal(:,nn,dd)))/runtimes;
        main_lobe_pow_uniform(nn,dd) = sum(squeeze(main_lobe_pow_data_uniform(:,nn,dd)))/runtimes;
    end
end
color_set = ['b';'r';'g';'c']; 
for nn=1:N_num
    [f,x] = ksdensity(squeeze(main_lobe_angle_data_normal(:,nn,5)));
    figure(99)
    subplot(221)
    plot(x,f,color_set(nn,:));hold on
    [f,x] = ksdensity(squeeze(main_lobe_angle_data_normal(:,nn,15)));
    figure(99)
    subplot(222)
    plot(x,f,color_set(nn,:));hold on
    [f,x] = ksdensity(squeeze(main_lobe_angle_data_uniform(:,nn,5)));
    figure(99)
    subplot(223)
    plot(x,f,color_set(nn,:));hold on
    [f,x] = ksdensity(squeeze(main_lobe_angle_data_uniform(:,nn,15)));
    figure(99)
    subplot(224)
    plot(x,f,color_set(nn,:));hold on
end

figure(99)
for nn=1:N_num
    subplot(2,2,nn)
    grid on
    legend('N_{BS} = 4','N_{BS} = 16','N_{BS} = 64','N_{BS} = 256');
    set(gca,'XTick',[-pi/12,-pi/24,0,pi/24,pi/12])
    set(gca,'XTickLabel',{'-15','-7.5','0','7.5','15'})
    xlim([-pi/12,pi/12])
    xlabel('Angle (degree)')
    ylabel('Empirical PDF')
end
%% lower bound
low_bound = 20*log10((1-0.5.*((1:15)./180.*pi).^2));

%%    
figure
plot_setting
plot(1:15,20*log10(main_lobe_pow_normal));hold on
plot(1:15,low_bound,'k--')
grid on
legend('N_{NS} = 4','N_{NS} = 16','N_{NS} = 64','N_{NS} = 256','Lower Bound')
xlabel('\Delta \phi_0 (degree)')
ylabel('\alpha (dB)')