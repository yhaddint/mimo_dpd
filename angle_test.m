clear;clc;
theta1 = linspace(-pi/2,pi/2,100);
theta2 = linspace(-pi/2,pi/2,100);
for ii=1:100
    for jj=1:100
        temp = 2*sin(theta1(ii))-sin(theta2(jj));
        if temp<-1
            theta0(ii,jj)=asin(temp+2);
        elseif temp>1
            theta0(ii,jj)=asin(temp-2);
        else
            theta0(ii,jj)=asin(temp);
        end
    end
end
%%
contourf(theta1,theta2,theta0)
colorbar