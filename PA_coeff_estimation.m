clear;clc;
M = 6;
s = (randn(M,1)+1j*randn(M,1))/sqrt(2);
beta1 = 10;
beta3 = -0.3;
beta = [beta1;beta3];
y = s;
env = sum(abs(y).^2);

% augmented symbol stream
for mm=1:M
    y_aug(mm,1) = y(mm);
    y_aug(mm,2) = 2*y(mm)*(sum(abs(y).^2)-abs(y(mm))^2)+y(mm)*abs(y(mm))^2;
end

% noise generation
noise_pow = 0.01;
noise = (randn(M,1)+1j*randn(M,1))*sqrt(noise_pow/2);

% PA nonlinear model
z = zeros(M,1);
for mm=1:M
    z(mm) = beta1*y(mm)+beta3*y(mm)*abs(y(mm))^2+beta3*2*y(mm)*(sum(abs(y).^2)-abs(y(mm))^2)+noise(mm);
end

%%
coeff_hat = pinv(y_aug)*z

