clear;clc;clear;close all
phi=1:15
figure
plot(phi,20*log10(1-0.5*(phi/180*pi).^2));hold on
xlabel('\Delta\phi_0 (degree)')
ylabel('\alpha (dB)')
grid on