clear;clc;
M = 4;
s = (randn(M,1)+1j*randn(M,1))/sqrt(2);
beta1 = 10;
beta3 = -0.3;
beta = [beta1;beta3];
y = s;
x = zeros(M,1);
h=zeros(M,M);

for mm=1:M
    phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2;s(3)*abs(s).^2;s(4)*abs(s).^2;];
end

% NLAP model
phi2 = zeros(M,2);
phi2(:,1) = x;
for mm=1:M
    phi2(mm,2) = x(mm)*abs(x(mm))^2+2*x(mm)*(sum(abs(x).^2)-abs(x(mm))^2);
end

%% LMS iteration
stepsize = 1e-5;
ite_num = 1e2;
runtimes = 1;
MSE = zeros(ite_num,runtimes);
for runindex = 1:runtimes
    % initialization of compensation weight
    w=zeros(M*(M+1),M);
    for mm=1:M
        w(mm,mm) = 1;
    end

    for ii = 1:ite_num
        s = s;
%         s = (randn(M,1)+1j*randn(M,1))/sqrt(2);
        for mm=1:M
            phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2;s(3)*abs(s).^2;s(4)*abs(s).^2;];
        end

        for mm=1:M
            x(mm) = w(:,mm).'*phi(:,mm);
        end 

        phi2(:,1) = x;
        for mm=1:M
            phi2(mm,2) = x(mm)*abs(x(mm))^2+2*x(mm)*(sum(abs(x).^2)-abs(x(mm))^2);
        end

        for mm=1:M
            for jj=1:M
                if mm==jj
                    h(mm,jj) = beta1+2*beta3*sum(abs(x).^2);
                else
                    h(mm,jj) = 2*beta3*x(mm)*conj(x(jj));
                end
            end
        end
        y = phi2*beta;
        error_vec = beta1*s-y;
        MSE(ii,runindex) = norm(error_vec)^2;
        for jj=1:M
            w(:,jj) = w(:,jj)+stepsize*phi(:,jj)*(error_vec'*h(:,jj));
        end
    %     w_vec = w_vec+stepsize*PHI*conj(error_vec);
    %     w = reshape(w_vec,M*(M+1),4);
    end
end
%%
figure
plot(mean(MSE,2))