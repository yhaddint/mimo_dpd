clear;%clc;
%% all parameters
EZmodel = 1;
rand('seed',1)
M = 2;
s = exp(1j*rand(M,1)*2*pi)/sqrt(M/2);
% s = [1+j;-1]/2;
beta1 = 10;
beta3 = -0.3;
beta = [beta1;beta3];
% y = s;
x = zeros(M,1);


% for mm=1:M
%     phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2;s(3)*abs(s).^2;s(4)*abs(s).^2;];
% %     phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2];
% end

% NLAP model
% phi2 = zeros(M,2);
% phi2(:,1) = x;
% for mm=1:M
%     phi2(mm,2) = x(mm)*abs(x(mm))^2+2*x(mm)*(sum(abs(x).^2)-abs(x(mm))^2);
% end

% analog beamforming and combining
Nt = 128;
AOD = linspace(-pi/5,pi/5,M).';
Frf = zeros(Nt,M);
for mm=1:M
    Frf(:,mm) = exp(1j*pi*(0:Nt-1).'*sin(AOD(mm)));
end

%% LMS iteration
stepsize = 5e-4;
ite_num = 5e2;

% h=zeros(ite_num,1);
h=zeros(M,M);
runtimes = 2e2;
MSE = zeros(ite_num,runtimes);
for runindex = 1:runtimes
    % initialization of compensation weight
    w=zeros(M+M^3,M);
    for mm=1:M
        w(mm,mm) = 1;
    end
    
    noise = (randn(M,ite_num)+randn(M,ite_num))*sqrt(0/2);
    for ii = 1:ite_num
%         s = s;
        s = exp(1j*rand(M,1)*2*pi)/sqrt(M/2);
        phi(1:M,1) = s;
%         for mm=1:M
%             phi(mm*M+1:(mm+1)*M,1) = [s(mm)*abs(s).^2];
% %             phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2];
%         end
        phi(M+1:M+M^3) = kron(kron(s,s),conj(s));

        for mm=1:M
            x(mm) = w(:,mm).'*phi;
        end
        
        
        if EZmodel
            % equivalent NL model in beamspace
            phi2(:,1) = x;
            for mm=1:M
                phi2(mm,2) = x(mm)*abs(x(mm))^2+2*x(mm)*(sum(abs(x).^2)-abs(x(mm))^2);
            end
            y = phi2*beta+noise(:,ii);
        else
            % Analog beamforming, NL PA, and combining
            x_PA_ip = Frf*x;
            x_PA_op = beta1*x_PA_ip+beta3*x_PA_ip.*abs(x_PA_ip).^2;
            y = Frf'*x_PA_op/Nt+noise(:,ii);
        end
        
        
        for mm=1:M
            for jj=1:M
                if mm==jj
                    h(mm,jj) = beta1+2*beta3*sum(abs(x).^2);
                else
                    h(mm,jj) = 2*beta3*x(mm)*conj(x(jj));
                end
            end
        end
%         h(ii) = beta1+2*beta3*sum(abs(x).^2);
        
        
        error_vec(:,ii) = beta1*s-y;
        MSE(ii,runindex) = norm(error_vec(:,ii))^2;
        for jj=1:M
            w(:,jj) = w(:,jj)+conj(phi)...
                *stepsize*((error_vec(:,ii)).'*h(:,jj));
        end
    %     w_vec = w_vec+stepsize*PHI*conj(error_vec);
    %     w = reshape(w_vec,M*(M+1),4);
    end
    
end
%%
% figure(1)
% plot(y,'o');hold on
% axis([-10,10,-10,10])
% grid on
%%
figure(3)
semilogy(mean(MSE,2));hold on
grid on
xlabel('Iteration Number')
ylabel('MSE')
%%
% figure(2)
% plot(10*log10(beta1^2./mean(MSE,2)));hold on
% grid on
% xlabel('Iteration Number')
% ylabel('SIR (dB)')