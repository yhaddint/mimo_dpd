clear;clc;
fs = 8e9;
XDelta = 1/fs;
%%
ZC_rate = 20e6;
ZC_seq = lteZadoffChuSeq(23,63);
ZC_oversamping_rate = fix(fs/ZC_rate);
% ZC_seq_oversamping = upsample(ZC_seq,4);
% t_origin = linspace(0,1,63);
% t_new = linspace(0,1,63*4);

t_ZC_org = linspace(0,63-1,63).';

t_ZC_new = linspace(0,63-1,63*ZC_oversamping_rate).';

ZC_seq_oversamping = interp1q(t_ZC_org, ZC_seq, t_ZC_new);

% figure
% plot(t_ZC_org,real(ZC_seq));hold on
% plot(t_ZC_new,real(ZC_seq_oversamping))
Y1 = repmat(ZC_seq_oversamping,3,1);
% figure;plot(abs(xcorr(Y,ZC_seq_oversamping)))
% figure
% plot(real(Y))
%%
sig_rate = 5e6;
sig_oversampling = fix(fs/sig_rate);
L = 1e4;
upsam = 80;
sig_intep_rate = sig_oversampling/upsam;
symbols = fix(L*2/upsam);   
hmod = modem.qammod('M', 4, 'InputType', 'integer');
hdesign  = fdesign.pulseshaping(upsam,'Square Root Raised Cosine');
hpulse = design(hdesign);
data = randi(4,symbols,1)-1;
data = modulate(hmod, data);
data = upsample(data,upsam);
temp_data = conv(data,hpulse.Numerator);
sig = temp_data(end-L+1:end)./sqrt(temp_data(end-L+1:end)'*temp_data(end-L+1:end)/L);
t_sig_org = linspace(0,length(sig)-1,length(sig)).';

t_sig_new = linspace(0,length(sig)-1,length(sig)*sig_intep_rate).';

Y2 = interp1q(t_sig_org, sig, t_sig_new);
Y = [Y1;Y2/sqrt(2)];
%%
figure
figure;plot(abs(xcorr(Y,ZC_seq_oversamping)))
