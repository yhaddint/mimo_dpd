function [ dphin] = shrinkage_v1( phi_n,phi_bar,deltaphi0 )
%SHRINKAGE_V1 Summary of this function goes here
%   Detailed explanation goes here
diff=phi_n-phi_bar;
if diff>0
    if diff <deltaphi0
        dphin = -phi_n+phi_bar;
       
    else
        dphin = -deltaphi0 ;
    end
else
    if diff<=-deltaphi0
        dphin = deltaphi0;
    else
        dphin = -phi_n+phi_bar;
    end
end

end

