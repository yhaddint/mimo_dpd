% 2015/09/10
% test whether my theory in sum rate degrading matches with simulations

clear;clc;clf;close all

UE_num = 2;
N_BS = 64;
N_MS = 16;
lambda = 3e8/60e9;
d=lambda/2;
SNR_num = 11;
SNR_dB_pool = linspace(-20,20,SNR_num);
SNR_pool = 10.^(SNR_dB_pool./10);
runtimes=1e4;
c=4.7e-18;
Ts = 1e-6;
fc = 60e9;
sigma2_PN = 0;

R_sum_perfect = zeros(SNR_num,runtimes);

rate_cdf_perfect = zeros(UE_num,runtimes);
rate_cdf_impair = zeros(UE_num,runtimes);


R_perfect = zeros(UE_num,SNR_num);
R_impair = zeros(UE_num,SNR_num);
R_impair_theory = zeros(UE_num,SNR_num);
    
for runindex=1:runtimes    
    % BS angle of depart
    psi = rand(UE_num,1)*pi;


    for uu=1:UE_num
        A_BS(:,uu) = exp(j*2*pi/lambda*d*(0:N_BS-1)*sin(psi(uu)))/sqrt(N_BS);
    end
    path_gain = (randn(UE_num,1)+j*randn(UE_num,1))/sqrt(2);
    D = sqrt(N_BS*N_MS)*diag(path_gain);
    
    DeltaPhi0 = pi/180*5;

    PS_impair_1 = diag(exp(j*(randn(1,N_BS)*DeltaPhi0)));
    PS_impair_2 = diag(exp(j*(randn(1,N_BS)*DeltaPhi0)));
%     PN_CO = diag(ones(1,UE_num)*exp(j*(rand()*2-1)*2*pi/10));
    F_RF = A_BS;


    H_bar = D*A_BS'*F_RF;
    H_bar_impair(1,1) = D(1,1)*A_BS(:,1)'*PS_impair_1*A_BS(:,1);
    H_bar_impair(1,2) = D(1,1)*A_BS(:,1)'*PS_impair_2*A_BS(:,2);
    H_bar_impair(2,1) = D(2,2)*A_BS(:,2)'*PS_impair_1*A_BS(:,1);
    H_bar_impair(2,2) = D(2,2)*A_BS(:,2)'*PS_impair_2*A_BS(:,2);

    
%     % test normalization matrix 
    temp = inv(A_BS'*A_BS);
    Eta=zeros(UE_num,UE_num);
    for uu=1:UE_num
        Eta(uu,uu)=sqrt(N_BS*N_MS/temp(uu,uu))*abs(path_gain(uu));
    end
    F_BB = H_bar'*inv(H_bar*H_bar')*Eta;
%     norm(A_BS*F_BB(:,2),2);
    
    eta_matrix = zeros(2);
    eta_matrix(1,2) = (randn+randn*j)*sqrt(1/N_BS/2)*DeltaPhi0;
    eta_matrix(2,1) = (randn+randn*j)*sqrt(1/N_BS/2)*DeltaPhi0;

    HF = H_bar*F_BB;
    HF_impair = H_bar_impair*F_BB;
    HF_impair_theory = (H_bar+D*eta_matrix)*F_BB;
    
    for uu=1:UE_num
        R_perfect(uu,:)=log2(1+abs(HF(uu,uu))^2./(norm(HF(uu,:))^2-abs(HF(uu,uu))^2+1./SNR_pool));
        R_impair(uu,:)=log2(1+abs(HF_impair(uu,uu))^2./(norm(HF_impair(uu,:))^2-abs(HF_impair(uu,uu))^2+1./SNR_pool));
        R_impair_theory(uu,:)=log2(1+abs(HF_impair_theory(uu,uu))^2./(norm(HF_impair_theory(uu,:))^2-abs(HF_impair_theory(uu,uu))^2+1./SNR_pool));

        
        rate_cdf_perfect(uu,runindex) = R_perfect(uu,end);
        rate_cdf_impair(uu,runindex) = R_impair(uu,end);
        rate_impair_theory(uu,runindex) = R_impair_theory(uu,end);
    end
    
    
    for ss=1:SNR_num
        R_sum_perfect(ss,runindex) = sum(R_perfect(:,ss));
        R_sum_impair(ss,runindex) = sum(R_impair(:,ss));
        R_sum_impair_theory(ss,runindex) = sum(R_impair_theory(:,ss));
    end
end
%% rate CDF
figure
hist(reshape(rate_cdf_perfect,UE_num*runtimes,1),20);hold on
figure
hist(reshape(rate_cdf_impair,UE_num*runtimes,1),20);hold on
% figure
% hist(reshape(rate_impair_theory,UE_num*runtimes,1),20);hold on
%%
[f,x] = ksdensity((reshape(rate_cdf_perfect,UE_num*runtimes,1)));
figure(98)
plot_setting
plot(x,f,'b');hold on
[f,x] = ksdensity((reshape(rate_cdf_impair,UE_num*runtimes,1)));
plot(x,f,'r');hold on
% [f,x] = ksdensity((reshape(rate_impair_theory,UE_num*runtimes,1)));
% plot(x,f,'g');hold on
grid on
xlabel('Mean Rate (bps/Hz)')
ylabel('Empirical PDF')
legend('Without PS Uncertainty','With PS Uncertainty')
%% figure
figure
plot_setting
plot(SNR_dB_pool,mean(R_sum_perfect,2)/UE_num,'b');hold on
plot(SNR_dB_pool,mean(R_sum_impair,2)/UE_num,'ro-');
plot(SNR_dB_pool,mean(R_sum_impair_theory,2)/UE_num,'r--');
xlabel('SNR (dB)')
ylabel('Spectral Efficiency (bps/Hz)')
legend('Without PS Uncertainty','With PS Uncertainty (Sim.)','With PS Uncertainty (Ana.)')
grid on






