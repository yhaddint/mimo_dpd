% 2016/09/05
% LMS based predistortion with memory depth 2

clear;%clc;
%% all parameters
EZmodel = 1;
% rand('seed',1)
M = 6;
% s = exp(1j*rand(M,1)*2*pi)/sqrt(M/2);
% s = [1+j;-1]/2;
beta1_0 = 1.0513+1j*0.0904;
beta3_0 = -0.0542-1j*0.2900;
beta1_1 = -0.0680-1j*0.0023;
beta3_1 = 0.1134+1j*0.1117;

% beta1_1 = 0;
% beta3_1 = 0;
% beta1_2 = 0.0289-1j*0.0054;
% beta3_2 = -0.0621-1j*0.0932;

% beta1_0 = 1.0513;
% beta3_0 = -0.0542;
% beta1_1 = 0
% beta3_1 = 0;
beta = [beta1_0;beta3_0;beta1_1;beta3_1];
% beta = [beta1_0;beta3_0;beta1_1;beta3_1;beta1_2;beta3_2];
% y = s;
x = zeros(M,1);


% for mm=1:M
%     phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2;s(3)*abs(s).^2;s(4)*abs(s).^2;];
% %     phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2];
% end

% NLAP model
% phi2 = zeros(M,2);
% phi2(:,1) = x;
% for mm=1:M
%     phi2(mm,2) = x(mm)*abs(x(mm))^2+2*x(mm)*(sum(abs(x).^2)-abs(x(mm))^2);
% end

% analog beamforming and combining
% Nt = 128;
% AOD = linspace(-pi/5,pi/5,M).';
% Frf = zeros(Nt,M);
% for mm=1:M
%     Frf(:,mm) = exp(1j*pi*(0:Nt-1).'*sin(AOD(mm)));
% end

%% LMS iteration
stepsize = 18e-3;
ite_num = 1.5e3;

h=zeros(ite_num,1);
h=zeros(M,M);
runtimes = 1e3;
MSE = zeros(ite_num,runtimes);
for runindex = 1:runtimes
    if mod(runindex,50)==0
        SIM_INFO = ['simulating',num2str(runindex/runtimes)];
        display(SIM_INFO)
    end
    
    % initialization of compensation weight
    w=zeros(2*(M+M^3),M);
    for mm=1:M
        w(mm,mm) = 1;
    end
    
    s = exp(1j*rand(M,ite_num)*2*pi)/sqrt(2)/sqrt(M);
%     s = ones(M,ite_num)/sqrt(M);
    for mm=1:M
        s_raw = ((randn(M,ite_num))+1j*(randn(M,ite_num)))/sqrt(2)/sqrt(M);
%         s_raw = rand(1,ite_num)*3;
        PAPR = 3.16;
%         PAPR = 1.5;
        index = find(abs(s_raw).^2<(PAPR/M));
        s(mm,index) = s_raw(index);
    end
%     max(abs(s_raw))^2
%     max(abs(s(1,:)))^2

    
    noise = (randn(M,ite_num)+1j*randn(M,ite_num))*sqrt(0.0/2);
    x = zeros(M,ite_num);
    y = zeros(M,ite_num);
    error_vec = zeros(M,ite_num);
    x(:,1) = s(:,1);
%     x(:,2) = s(:,2);
    for nn = 2:ite_num
%         s = s;
        phi(1:M,1) = s(:,nn);
%         for mm=1:M
%             phi(mm*M+1:(mm+1)*M,1) = [s(mm)*abs(s).^2];
% %             phi(:,mm) = [s;s(1)*abs(s).^2;s(2)*abs(s).^2];
%         end
        phi(M+1:M+M^3,1) = kron(kron(s(:,nn),s(:,nn)),conj(s(:,nn)));
        phi(M+M^3+1:M+M^3+M,1) = s(:,nn-1);
        phi(M+M^3+M+1:2*(M+M^3)) = kron(kron(s(:,nn-1),s(:,nn-1)),conj(s(:,nn-1)));
%         phi(2*(M+M^3)+1:2*(M+M^3)+M,1) = s(:,nn-2);
%         phi(2*(M+M^3)+M+1:3*(M+M^3)) = kron(kron(s(:,nn-2),s(:,nn-2)),conj(s(:,nn-2)));


        for mm=1:M
            x(mm,nn) = w(:,mm)'*phi;
        end
        
        
        if EZmodel
            % equivalent NL model in beamspace
            phi2(:,1) = x(:,nn);
            phi2(:,3) = x(:,nn-1);
%             phi2(:,5) = x(:,nn-2);
            for mm=1:M
                phi2(mm,2) = x(mm,nn)*abs(x(mm,nn))^2+...
                    2*x(mm,nn)*(sum(abs(x(:,nn)).^2)-abs(x(mm,nn))^2); 
                    
                phi2(mm,4) = x(mm,nn-1)*abs(x(mm,nn-1))^2+...
                    2*x(mm,nn-1)*(sum(abs(x(:,nn-1)).^2)-abs(x(mm,nn-1))^2); 
                
%                 phi2(mm,6) = x(mm,nn-2)*abs(x(mm,nn-2))^2+...
%                     2*x(mm,nn-2)*(sum(abs(x(:,nn-2)).^2)-abs(x(mm,nn-2))^2); 
            end
            y(:,nn) = phi2*beta;%+noise(:,nn);
            
            % non-DPD symbols
            phi2_s(:,1) = s(:,nn);
            phi2_s(:,3) = s(:,nn-1);
            for mm=1:M
                phi2_s(mm,2) = s(mm,nn)*abs(s(mm,nn))^2+...
                    2*s(mm,nn)*(sum(abs(s(:,nn)).^2)-abs(s(mm,nn))^2); 
                    
                phi2_s(mm,4) = s(mm,nn-1)*abs(s(mm,nn-1))^2+...
                    2*s(mm,nn-1)*(sum(abs(s(:,nn-1)).^2)-abs(s(mm,nn-1))^2); 
                y_noDPD(:,nn) = phi2_s*beta;
            end
        else
            % Analog beamforming, NL PA, and combining
%             x_PA_ip = Frf*x;
%             x_PA_op = beta1*x_PA_ip+beta3*x_PA_ip.*abs(x_PA_ip).^2;
%             y(:,nn) = Frf'*x_PA_op/Nt;%+noise(:,nn);
        end
        
        % calculate error vector and MSE
        error_vec(:,nn) = s(:,nn)-y(:,nn);
        MSE(nn,runindex) = norm(error_vec(:,nn))^2;
        
        MSE_noDPD(nn,runindex) = norm(s(:,nn)-y_noDPD(:,nn))^2;
        
        % LMS updating based on partial derivative of r_k[n] to x_l[n-d]
        % d = 0 part
        for kk=1:M
            for ll=1:M
                if kk==ll
                    h(ll,kk) = beta1_0+2*beta3_0*sum(abs(x(:,nn)).^2);
                else
                    h(ll,kk) = 2*beta3_0*x(ll,nn)*conj(x(kk,nn));
                end
            end
        end
        for mm=1:M
            w(1:(M+M^3),mm) = w(1:(M+M^3),mm)+stepsize*(error_vec(:,nn))'*h(mm,:).'...
                *(phi(1:(M+M^3)));
        end

        % d = 1 part
        for kk=1:M
            for ll=1:M
                if kk==ll
                    h(ll,kk) = beta1_0+2*beta3_0*sum(abs(x(:,nn-1)).^2);
                else
                    h(ll,kk) = 2*beta3_0*x(ll,nn-1)*conj(x(kk,nn-1));
                end
            end
        end
        for mm=1:M
            w((M+M^3+1):2*(M+M^3),mm) = w((M+M^3+1):2*(M+M^3),mm)+stepsize*(error_vec(:,nn))'*h(mm,:).'...
                *(phi((M+M^3+1):2*(M+M^3)));
        end
        
        % d = 2 part
%         for kk=1:M
%             for ll=1:M
%                 if kk==ll
%                     h(ll,kk) = beta1_0+2*beta3_0*sum(abs(x(:,nn-2)).^2);
%                 else
%                     h(ll,kk) = 2*beta3_0*x(ll,nn-2)*conj(x(kk,nn-2));
%                 end
%             end
%         end
%         for mm=1:M
%             w(2*(M+M^3)+1:3*(M+M^3),mm) = w(2*(M+M^3)+1:3*(M+M^3),mm)+stepsize*(error_vec(:,nn))'*h(mm,:).'...
%                 *(phi(2*(M+M^3)+1:3*(M+M^3)));
%         end

    %     w_vec = w_vec+stepsize*PHI*conj(error_vec);
    %     w = reshape(w_vec,M*(M+1),4);
    end
    
end
%%
% figure(1)
% plot(y,'o');hold on
% axis([-10,10,-10,10])
% grid on
%%
figure(3)
semilogy(mean(MSE,2));hold on
semilogy(mean(MSE_noDPD,2));hold on
grid on
xlabel('Training Length')
ylabel('Mean Square Error')
%%
% figure(2)
% plot(10*log10(beta1^2./mean(MSE,2)));hold on
% grid on
% xlabel('Iteration Number')
% ylabel('SIR (dB)')