% 2015/08/23
% addint phase 
clear;clc
N_BS = 64;
Z_matrix = diag(ones(1,N_BS)*50);
Z_matrix = Z_matrix+diag(ones(1,N_BS-1)*(-10-10j),1)+diag(ones(1,N_BS-1)*(-10-10j),-1);
C_matrix = inv(diag(ones(1,N_BS))*50+Z_matrix)*50;

UE_num = 4;
N_MS = 16;
lambda = 3e8/60e9;
d=lambda/2;
SNR_num = 11;
SNR_dB_pool = linspace(-20,20,SNR_num);
SNR_pool = 10.^(SNR_dB_pool./10);
runtimes=1e3;
c=4.7e-18;
Ts = 1e-6;
fc = 60e9;
sigma2_PN = 4*pi^2*fc^2*c*Ts;

R_sum_perfect = zeros(SNR_num,runtimes);

for runindex=1:runtimes
    
    R_perfect = zeros(UE_num,SNR_num);
    
    % BS angle of depart
    psi = rand(UE_num,1)*2*pi;
    for uu=1:UE_num
        A_BS(:,uu) = exp(j*2*pi/lambda*d*linspace(0,N_BS-1)*sin(psi(uu)))/sqrt(N_BS);
    end
    path_gain = (randn(UE_num,1)+j*randn(UE_num,1))/sqrt(2);
    D = sqrt(N_BS*N_MS)*diag(path_gain);
    H_bar = D*A_BS'*A_BS;
%     PN_DO = diag(exp(j*randn(1,UE_num)*sqrt(sigma2_PN)));
%     PN_CO = diag(ones(1,UE_num)*exp(j*(rand()*2-1)*2*pi/10));

    
%     % test normalization matrix 
    temp = inv(A_BS'*A_BS);
    Eta=zeros(UE_num,UE_num);
    for uu=1:UE_num
        Eta(uu,uu)=sqrt(N_BS*N_MS/temp(uu,uu))*abs(path_gain(uu));
    end
    F_BB = H_bar'*inv(H_bar*H_bar')*Eta;
%     norm(A_BS*F_BB(:,2),2);
        
    
    
    for uu=1:UE_num
        temp = inv(A_BS'*A_BS);
        R_perfect(uu,:)=log2(1+SNR_pool/UE_num/temp(uu,uu)*N_BS*N_MS*abs(path_gain(uu))^2);
        
         imp_matrix_coup = H_bar*C_matrix*H_bar'*inv(H_bar*H_bar')*Eta;
         inter_user_coup = imp_matrix_coup(uu,:);
         R_imperfect_coup(uu,:)=log2(1+abs(inter_user_coup(uu))^2./((UE_num./SNR_pool)+(norm(inter_user_coup)^2-abs(inter_user_coup(uu))^2)));
    
%         imp_matrix_CO = H_bar*PN_CO*H_bar'*inv(H_bar*H_bar')*Eta;
%         inter_user_CO = imp_matrix_CO(uu,:);
%         R_imperfect_CO(uu,:)=log2(1+abs(inter_user_CO(uu))^2./((UE_num./SNR_pool)+(norm(inter_user_CO)^2-abs(inter_user_CO(uu))^2)));
%  
    end
    for ss=1:SNR_num
        R_sum_perfect(ss,runindex) = sum(R_perfect(:,ss));
         R_sum_imperfect_coup(ss,runindex) = sum(R_imperfect_coup(:,ss));
        %R_sum_imperfect_CO(ss,runindex) = sum(R_imperfect_CO(:,ss));
    end
end

%% figure
figure
plot_setting
plot(SNR_dB_pool,mean(R_sum_perfect,2)/UE_num,'b-o');hold on
plot(SNR_dB_pool,mean(R_sum_imperfect_coup,2)/UE_num,'r-s');
%plot(SNR_dB_pool,mean(R_sum_imperfect_CO,2)/UE_num,'g-^');
xlabel('SNR (dB)')
ylabel('Spectral Efficiency (bps/Hz)')
legend('Without Antenna Coupling','With Antenna Coupling (d=\lambda/2)')
grid on