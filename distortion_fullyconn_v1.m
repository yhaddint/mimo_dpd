clear;clc;
M_range = [2,4,8,16];
Nt = 128;
MCtimes = 5e2;
ang_grid_num = 540;
ang_grid = linspace(-pi/2,pi/2,ang_grid_num);
noise_pow = 0.1;
%% Fully-connected architecture
beta3 = -3;

for MCindex = 1:MCtimes
    AOD_index = 90+randi(360,max(M_range),1);
    AOD = ang_grid(AOD_index);
    for mm=1:max(M_range)
        steervec(:,mm) = exp(1j*pi*sin(AOD(mm))*(0:Nt-1))/sqrt(Nt);
    end
    sig_symbols = (randn(max(M_range),1)+1j*randn(max(M_range),1))/sqrt(2);
    
    for rr=1:4
        M = M_range(rr);
        
        y_L = steervec(:,1:M)*sig_symbols(1:M);
        y_NL = y_L+beta3*y_L.*y_L.*conj(y_L);
        
        for mm=1:M
            ang_res_L(mm) = steervec(:,mm)'*y_L;
            ang_res_NL(mm) = steervec(:,mm)'*y_NL;
        end
        EV_L_FC(MCindex,rr) = norm(ang_res_L(1:M).'-sig_symbols(1:M))^2/M;
        EV_NL_FC(MCindex,rr) = norm(ang_res_NL(1:M).'-sig_symbols(1:M))^2/M;
        

%         SINR_L_FC(MCindex,rr) = norm(ang_res_L(1:M))/norm(ang_res_L(1:M).'+noise-sig_symbols(1:M));
%         SINR_NL_FC(MCindex,rr) = norm(ang_res_NL(1:M))/norm(ang_res_NL(1:M).'+noise-sig_symbols(1:M));
    end
end

%% sub-array architecture
beta3 = -3;

for MCindex = 1:MCtimes
    AOD_index = 90+randi(360,max(M_range),1);
    AOD = ang_grid(AOD_index);
    for mm=1:max(M_range)
        steervec(:,mm) = exp(1j*pi*sin(AOD(mm))*(0:Nt-1))/sqrt(Nt);
    end
    sig_symbols = (randn(max(M_range),1)+1j*randn(max(M_range),1))/sqrt(2);
    
    for rr=1:4
        M = M_range(rr);
        steervec_SA = zeros(Nt,M);
        for mm=1:M
            ant = (mm-1)*(Nt/M)+1:mm*Nt/M;
            steervec_SA(ant,mm) = exp(1j*pi*sin(AOD(mm))*(ant-1))/sqrt(Nt/M);
        end
        y_L = steervec_SA(:,1:M)*sig_symbols(1:M);
        y_NL = y_L+beta3*y_L.*y_L.*conj(y_L);
        for mm=1:M
            ang_res_L(mm) = steervec(:,mm)'*y_L;
            ang_res_NL(mm) = steervec(:,mm)'*y_NL;
        end
        
        EV_L_SA(MCindex,rr) = norm(ang_res_L(1:M).'-sig_symbols(1:M)/sqrt(M))^2/M;
        EV_NL_SA(MCindex,rr) = norm(ang_res_NL(1:M).'-sig_symbols(1:M)/sqrt(M))^2/M;
        
%         noise = (randn(M,1)+1j*randn(M,1))*sqrt(noise_pow/2);
%         S_L_SA(MCindex,rr) = norm(ang_res_L(1:M))res_L(1:M).'+noise-sig_symbols(1:M)/sqrt(M));
%         S_NL_SA(MCindex,rr) = norm(ang_res_NL(1:M))/norm(ang_res_NL(1:M).'+noise-sig_symbols(1:M)/sqrt(M));
    end
end
%%
figure(1)
semilogy(M_range,mean(EV_L_FC,1),'-o','linewidth',3);hold on
semilogy(M_range,mean(EV_NL_FC,1),'-o','linewidth',3);hold on
semilogy(M_range,mean(EV_L_SA,1),'--s','linewidth',3);hold on
semilogy(M_range,mean(EV_NL_SA,1),'--s','linewidth',3);hold on
grid on
xlabel('Number of Beams')
ylabel('Mean square distortion')
legend('FC, Linear','FC, Nonlinear','SA, Linear','SA, Nonlinear')
%%
figure(2)
plot(M_range,10*log10(1./(noise_pow+mean(EV_L_FC,1))),'-o','linewidth',3);hold on
plot(M_range,10*log10(1./(noise_pow+mean(EV_NL_FC,1))),'-o','linewidth',3);hold on
plot(M_range,10*log10(1./M_range./(noise_pow+mean(EV_L_SA,1))),'--s','linewidth',3);hold on
plot(M_range,10*log10(1./M_range./(noise_pow+mean(EV_NL_SA,1))),'--s','linewidth',3);hold on
grid on
xlabel('Number of Beams')
ylabel('Mean square distortion')
legend('FC, Linear','FC, Nonlinear','SA, Linear','SA, Nonlinear')
% figure
% plot(ang_grid,20*log10(abs(ang_res_L)));hold on
% plot(ang_grid,20*log10(abs(ang_res_NL)));
% grid on
% legend('Linear PA','nonlinear PA')
% ylim([-10,25])
% % end