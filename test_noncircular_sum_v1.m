clear;clc;clf;close all
N=64;

alpha=exp(j*((1:N)-1)*(sin(pi/2)-sin(pi/3))*pi);
runtimes = 1e4;
deltaphi0 = 5/180*pi;
for runindex=1:runtimes
    data(runindex) = j*sum(randn(1,N).*deltaphi0.*alpha);
    analy(runindex) = (randn+randn*j)*sqrt(N/2)*deltaphi0;
end
%%
figure
plot(data,'.');hold on
plot((sum(alpha)),'ro')
grid
axis([-3,3,-3,3])
% figure
% plot((sum(alpha)+data),'r.')
% axis([-20,20,-20,20])
% grid on
figure
plot(analy,'r.')
axis([-3,3,-3,3])
%%
% figure
% hist(abs(data),40)