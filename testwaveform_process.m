clear;clc;
addpath('/Users/hanyan/Desktop');
row_offset = 0;
col_offset = 3;
M = csvread('zc_testform_out001.csv',row_offset,col_offset);
%%
fs = 1e9;
sig = M(:,2);
sig_real = sig.*cos(-2*pi*140e6/fs*(0:1e5-1).');
sig_imag = sig.*sin(-2*pi*140e6/fs*(0:1e5-1).');

% sig_comp = sig_real+1j*sig_imag;
bhi0 = fir1(200,0.25,'low');
%     freqz(bhi0)
temp = filter(bhi0,1,sig_real);
sig_real_filter = temp(101:end);

temp = filter(bhi0,1,sig_imag);
sig_imag_filter = temp(101:end);

sig_legnth = length(sig_imag_filter);

sig_comp = sig_real_filter + 1j* sig_imag_filter;
%%
ydata = abs(fft(sig_comp));
xdata = linspace(-5e2,5e2,sig_legnth);
figure;plot(xdata,([ydata(sig_legnth/2+1:end);ydata(1:sig_legnth/2)]))
xlabel('Freq (MHz)')
%%
% figure;
% plot(abs(sig_comp))

%%
ZC_rate = 20e6;
ZC_seq = lteZadoffChuSeq(23,63);
ZC_oversamping_rate = fix(fs/ZC_rate);
% ZC_seq_oversamping = upsample(ZC_seq,4);
% t_origin = linspace(0,1,63);
% t_new = linspace(0,1,63*4);
t_ZC_org = linspace(0,63-1,63).';
t_ZC_new = linspace(0,63-1,63*ZC_oversamping_rate).';
ZC_seq_oversamping = interp1q(t_ZC_org, ZC_seq, t_ZC_new);
%%
figure
figure;plot(abs(xcorr(sig_comp(1:end),ZC_seq_oversamping)))
%%
sig_comp_shift = sig_comp(33738:end);
load('han_testwave')
sig_org = Y(1:8:end);
figure;
plot(10*abs(sig_comp_shift(1:34450)),'r');hold on
plot(abs(sig_org),'b','linewidth',1)
legend('output','input')

% data = [abs(sig_org;5*abs(sig_comp_shift(1:34450)];
%% AM/AM
maxorg = max(abs(sig_org(1e4:34450)));
maxout = max(abs(sig_comp_shift(1e4:34450)));

figure(99)
subplot(211)
plot(abs(sig_org(1e4:34450))/maxorg,abs(sig_comp_shift(1e4:34450))/maxout,'.')
axis([0,1,0,1])
%% AM/PM 
angle_diff = phase(sig_comp_shift(1e4:32000).*conj(sig_org(1e4:32000)));
Df = (angle_diff(22000)-angle_diff(1))/(22000);
angle_diff_zeromean = angle_diff-angle_diff(1)-Df*(0:22000).';
figure(99)
subplot(212)
plot(abs(sig_org(1e4:32000))/maxorg,angle_diff_zeromean,'.')
%% MP parameter extraction (NL order = 5, D = 3)
% s(N), s(N-1), s(N-2),...
% s(N)|s(N)|^2, s(N-1)|s(N-1)|^2, s(N-2)|s(N-2)|^2,...
% s(N)|s(N)|^4, s(N-1)|s(N-1)|^4, s(N-2)|s(N-2)|^4

sig_in = sig_org(1e4:32000);
sig_out = sig_comp_shift(1e4:32000).*exp(-1j*angle_diff(1)).*exp(-1j*Df*(0:22000).');

% angle_diff2 = phase(sig_out.*conj(sig_in));
% figure
% plot(abs(sig_in)/maxorg,angle_diff2,'.')
%
L_train = 5000;
sig_d0 = sig_in(3:end);
sig_d1 = sig_in(2:end-1);
sig_d2 = sig_in(1:end-2);
%----------- First Order ----------------
A_10 = sig_d0;
A_11 = sig_d1;
A_12 = sig_d2;

%----------- Third Order ----------------
A_30 = sig_d0 .* sig_d0 .* conj(sig_d0);
A_31 = sig_d1 .* sig_d1 .* conj(sig_d1);
A_32 = sig_d2 .* sig_d2 .* conj(sig_d2);

%----------- Fifth Order ----------------
A_50 = sig_d0 .* sig_d0.^2 .* conj(sig_d0).^2;
A_51 = sig_d1 .* sig_d1.^2 .* conj(sig_d1).^2;
A_52 = sig_d2 .* sig_d2.^2 .* conj(sig_d2).^2;

%----------- A matrix -------------
A = [A_10, A_11, A_12, A_30, A_31, A_32, A_50, A_51, A_52];
alpha = pinv(A(1:L_train,:))*sig_out(3:L_train+2);
%%
L = 2e4;
ydata = abs(fft(hann(L).*sig_out(3:L+2)));
xdata = linspace(-5e2,5e2,L).';
figure

plot(xdata,20*log10([ydata(L/2+1:end);ydata(1:L/2)]),'linewidth',2);hold on
ydata = abs(fft(hann(L).*(sig_out(3:L+2) - A(1:L,:)*alpha)));
plot(xdata,20*log10([ydata(L/2+1:end);ydata(1:L/2)]),'linewidth',2);hold on
ydata = abs(fft(hann(L).*sig_in(3:L+2)));
plot(xdata,-19+20*log10([ydata(L/2+1:end);ydata(1:L/2)]),'linewidth',2);hold on

legend('Spectrum of Distorted Output','Spectrum of Modeling Error','Spectrum of Input')
xlim([-20,20])
ylim([-20,50])
grid on
xlabel('Freq (MHz)')
ylabel('Spectrum (dB)')
%% Post-compensation AM/AM & AM/PM
