clear;clc;
M = 8;
Nt = 64;
MCtimes = 1e3;
AOD = rand(M,1)*pi/3*2-pi/3;
%%
beta3 = -1;

% for MCindex = 1:MCtimes

for mm=1:M
    steervec(:,mm) = exp(1j*pi*sin(AOD(mm))*(0:Nt-1))/sqrt(Nt);
end
y_L = sum(steervec,2);
y_NL = sum(steervec,2)+beta3*sum(steervec,2).*sum(steervec,2).*conj(sum(steervec,2));

ang_grid_num = 600;
ang_grid = linspace(-pi/2,pi/2,ang_grid_num);
for ii=1:ang_grid_num
    ang_channel(:,ii) = exp(1j*pi*sin(ang_grid(ii))*(0:Nt-1).');
    ang_res_L(ii) = ang_channel(:,ii)'*y_L;
    ang_res_NL(ii) = ang_channel(:,ii)'*y_NL;
end
%%
figure
plot(ang_grid,20*log10(abs(ang_res_L)));hold on
plot(ang_grid,20*log10(abs(ang_res_NL)));
grid on
legend('Linear PA','nonlinear PA')
ylim([-10,25])
% end