% 2015/08/23
% addint phase drift into each RF chain

clear;clc;%clf;close all
UE_num = 2;
N_BS = 16;
N_MS = 16;
lambda = 3e8/60e9;
d=lambda/2;
SNR_num = 11;
SNR_dB_pool = linspace(-20,20,SNR_num);
SNR_pool = 10.^(SNR_dB_pool./10);
runtimes=1e3;
c=4.7e-18;
Ts = 1e-6;
fc = 60e9;
sigma2_PN = 0;

R_sum_perfect = zeros(SNR_num,runtimes);

rate_cdf_perfect = zeros(UE_num,runtimes);
rate_cdf_impair = zeros(UE_num,runtimes);

for runindex=1:runtimes
    
    R_perfect = zeros(UE_num,SNR_num);
    
    % BS angle of depart
    psi = rand(UE_num,1)*2*pi;
    for uu=1:UE_num
        A_BS(:,uu) = exp(j*2*pi/lambda*d*(0:N_BS-1)*sin(psi(uu)))/sqrt(N_BS);
    end
    path_gain = (randn(UE_num,1)+j*randn(UE_num,1))/sqrt(2);
    D = sqrt(N_BS*N_MS)*diag(path_gain);
    
    aa=pi/180*10;
    bb=pi/180*5;
    
    PS_impair_1 = diag(exp(j*(rand(1,N_BS)*aa-bb)));
    PS_impair_2 = diag(exp(j*(rand(1,N_BS)*aa-bb)));
%     PN_CO = diag(ones(1,UE_num)*exp(j*(rand()*2-1)*2*pi/10));
    F_RF = A_BS;
    H_bar_worst(1,1) = (1+j*pi/180*5)*D(1,1)*A_BS(:,1)'*A_BS(:,1);
    H_bar_worst(1,2) = (1+j*pi/180*5)*D(1,1)*A_BS(:,1)'*A_BS(:,2);
    H_bar_worst(2,1) = (1+j*pi/180*5)*D(2,2)*A_BS(:,2)'*A_BS(:,1);
    H_bar_worst(2,2) = (1+j*pi/180*5)*D(2,2)*A_BS(:,2)'*A_BS(:,2);

    H_bar = D*A_BS'*F_RF;
    H_bar_impair(1,1) = D(1,1)*A_BS(:,1)'*PS_impair_1*A_BS(:,1);
    H_bar_impair(1,2) = D(1,1)*A_BS(:,1)'*PS_impair_2*A_BS(:,2);
    H_bar_impair(2,1) = D(2,2)*A_BS(:,2)'*PS_impair_1*A_BS(:,1);
    H_bar_impair(2,2) = D(2,2)*A_BS(:,2)'*PS_impair_2*A_BS(:,2);

    
%     % test normalization matrix 
    temp = inv(A_BS'*A_BS);
    Eta=zeros(UE_num,UE_num);
    for uu=1:UE_num
        Eta(uu,uu)=sqrt(N_BS*N_MS/temp(uu,uu))*abs(path_gain(uu));
    end
    F_BB = H_bar'*inv(H_bar*H_bar')*Eta;
%     norm(A_BS*F_BB(:,2),2);



    F_BB_worst = H_bar_worst'*inv(H_bar_worst*H_bar_worst');
    
    HF = H_bar*F_BB;
    HF_impair = H_bar_impair*F_BB;
    HF_worst = H_bar_impair*F_BB_worst;
    
    for uu=1:UE_num
        R_perfect(uu,:)=log2(1+abs(HF(uu,uu))^2./(norm(HF(uu,:))^2-abs(HF(uu,uu))^2+1./SNR_pool));
        R_impair(uu,:)=log2(1+abs(HF_impair(uu,uu))^2./(norm(HF_impair(uu,:))^2-abs(HF_impair(uu,uu))^2+1./SNR_pool));
        R_worst(uu,:)=log2(1+abs(HF_worst(uu,uu))^2./(norm(HF_worst(uu,:))^2-abs(HF_worst(uu,uu))^2+1./SNR_pool));

        
        rate_cdf_perfect(uu,runindex) = R_perfect(uu,end);
        rate_cdf_impair(uu,runindex) = R_impair(uu,end);
    end
    

    
    for ss=1:SNR_num
        R_sum_perfect(ss,runindex) = sum(R_perfect(:,ss));
        R_sum_impair(ss,runindex) = sum(R_impair(:,ss));
        R_sum_worst(ss,runindex) = sum(R_worst(:,ss));
    end
end
%% rate CDF
figure
plot(ecdf(reshape(rate_cdf_perfect,UE_num*runtimes,1)));hold on
plot(ecdf(reshape(rate_cdf_impair,UE_num*runtimes,1)));hold on

%% figure
figure
plot_setting
plot(SNR_dB_pool,mean(R_sum_perfect,2)/UE_num,'b-o');hold on
plot(SNR_dB_pool,mean(R_sum_impair,2)/UE_num,'r-s');
plot(SNR_dB_pool,mean(R_sum_worst,2)/UE_num,'g-^');
xlabel('SNR (dB)')
ylabel('Spectral Efficiency (bps/Hz)')
legend('Without PS Impairment','With PS Impairment')
grid on




